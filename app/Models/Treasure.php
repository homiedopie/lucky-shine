<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treasure extends Model
{
    /**
     * Create scope distance
     * Reference: https://www.movable-type.co.uk/scripts/latlong-db.html
     *
     * @param Query $query
     * @param float $latitude
     * @param float $longitude
     * @param integer $distanceInKm
     * @return Query
     */
    public function scopeDistance($query, $latitude, $longitude, $distanceInKm)
    {
        $earthRadiusKm = 6371;
        $kmInLatDegree = rad2deg($distanceInKm/$earthRadiusKm);
        $kmInLongDegree = rad2deg(asin($distanceInKm/$earthRadiusKm) / cos(deg2rad($latitude)));

        $maxLat = $latitude + $kmInLatDegree;
        $minLat = $latitude - $kmInLatDegree;
        $maxLon = $longitude + $kmInLongDegree;
        $minLon = $longitude - $kmInLongDegree;
        $table = 'treasures';

        return $query
            ->addSelect(\DB::raw('*')) // This will ignore the fields checking defaulting results to all columns
            ->addSelect(
                \DB::raw("
                    (6371 * acos(cos(radians($latitude)) 
                    * cos(radians({$table}.latitude)) 
                    * cos(radians({$table}.longitude) 
                    - radians({$longitude}))
                    + sin(radians({$latitude})) 
                    * sin(radians({$table}.latitude)))) as distance
                ")
            )
            ->whereBetween('latitude', [$minLat, $maxLat])
            ->whereBetween('longitude', [$minLon, $maxLon])
            ->having('distance', '<=', $distanceInKm);
    }

    /**
     * Get the money values for the treasure.
     */
    public function money_values()
    {
        return $this->hasMany('App\Models\MoneyValue');
    }
}
