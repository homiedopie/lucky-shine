<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    /**
     * Get the treasure associated for the collection.
     */
    public function treasure()
    {
        return $this->belongsTo('App\Models\Treasure');
    }

    /**
     * Get the money value associated for the collection.
     */
    public function money_value()
    {
        return $this->belongsTo('App\Models\MoneyValue');
    }

    /**
     * Get the user associated for the collection.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * Get collection within the day
     *
     * @param Query $query
     * @param integer $user_id
     * @return Query
     */
    public function scopeWithinTheDay($query, $user_id, $treasure_id)
    {
        return $query
            ->where('treasure_id', $treasure_id)
            ->where('collected_at', \Carbon\Carbon::now()->startOfDay())
            ->where('user_id', $user_id);
    }
}
