<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MoneyValue extends Model
{
    /**
     * Get the treasure associated for the money value.
     */
    public function treasure()
    {
        return $this->belongsTo('App\Models\Treasure');
    }
}
