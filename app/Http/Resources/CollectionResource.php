<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CollectionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->user),
            'treasure' => new TreasureResource($this->treasure),
            'money_value' => new MoneyValueResource($this->money_value),
            'collected_at' => $this->collected_at
        ];
    }
}
