<?php

namespace App\Http\Controllers;

use App\Models\Treasure;
use App\Models\Collection;
use App\Http\Requests\GetTreasureRequest;
use App\Http\Requests\CollectTreasureRequest;
use App\Http\Resources\TreasureResource;
use App\Http\Resources\CollectionResource;
use Illuminate\Http\Request;

class TreasureController extends Controller
{
    /**
     * Get treasures with filters
     * TODO: Integrate user in filter
     *
     * @param GetTreasureRequest $request
     * @return TreasureResource
     */
    public function index(GetTreasureRequest $request)
    {
        $latitude = $request->input('latitude');
        $longitude = $request->input('longitude');
        $distance = $request->input('distance');
        $prize_value = $request->input('prize_value');

        // TODO: Add a repository
        $treasure_query = Treasure::with(
            'money_values'
        )->distance($latitude, $longitude, $distance);
            
        if ($prize_value) {
            $treasure_query
                ->whereDoesntHave('money_values', function ($query) use ($prize_value) {
                    $query->where('amount', '<', $prize_value);
                });
        }

        return TreasureResource::collection($treasure_query->get());
    }

    /**
     * Collect treasure
     *
     * @param Request $request
     * @param Treasure $treasure
     * @return TreasureResource
     */
    public function collect(CollectTreasureRequest $request, Treasure $treasure)
    {
        $to_latitude = $request->input('latitude');
        $to_longitude = $request->input('longitude');
        $user_id = $request->input('user_id');
        $metersToKm = 10 / 1000;

        // Check if the user is within the proximity of the treasure
        if (
            !$this->isNearby(
                $treasure->latitude,
                $treasure->longitude,
                $to_latitude,
                $to_longitude,
                $metersToKm
            )
        ) {
            // TODO: Add error resource
            // TODO: Add to a form validator
            return response()->json([
                'error' => 'The User is not range.',
            ], 422);
        }

        // Check if the user has already collected the treasure for the day
        if (\App\Models\Collection::withinTheDay($user_id, $treasure->id)->first()) {
            return response()->json([
                'error' => 'The User has already collected the treasure for the day.',
            ], 422);
        }
        
        // Check if treasure has money values
        if (!$treasure->money_values->count()) {
            return response()->json([
                'error' => 'Invalid treasure.',
            ], 422);
        }

        // Pick one from the money values
        $money_value = $treasure->money_values->random();
        // Check if the money value selected has an amount
        // TODO: Track attempt for each user to avoid exhausting all possible money values
        if (!$money_value->amount) {
            return response()->json([
                'error' => 'Invalid treasure.',
            ], 422);
        }

        // TODO: Mass assignment
        $collection = new Collection();
        $collection->user_id = $user_id;
        $collection->treasure_id = $treasure->id;
        $collection->money_value_id = $money_value->id;
        $collection->amount = $money_value->amount;
        $collection->collected_at = \Carbon\Carbon::now()->startOfDay();
        $collection->save();

        return new CollectionResource($collection);
    }

    /**
     * Calculate proximity from and to distance with range
     * TODO: Move in helper
     * @param float $from_latitude
     * @param float $from_longitude
     * @param float $to_latitude
     * @param float $to_longitude
     * @param float $rangeInKm
     * @return boolean
     */
    private function isNearby(
        $from_latitude,
        $from_longitude,
        $to_latitude,
        $to_longitude,
        $rangeInKm
    ) {
        $distance = (
            6371 *
            acos(
                (cos(deg2rad($from_latitude))) *
                (cos(deg2rad($to_latitude))) *
                (cos(deg2rad($to_longitude) - deg2rad($from_longitude))) +
                ((sin(deg2rad($from_latitude))) * (sin(deg2rad($to_latitude))))
            )
        );
        return $distance <= $rangeInKm;
    }
}
