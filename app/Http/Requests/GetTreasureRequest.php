<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class GetTreasureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'distance' => 'required|in:1,10',
            'prize_value' => 'integer|between:10,30'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'latitude' => 'Latitude',
            'longitude' => 'Longitude',
            'distance' => 'Distance',
            'prize_value' => 'Prize Value'
        ];
    }
}
