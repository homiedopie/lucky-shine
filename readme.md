# Lucky Shine Application

Requirements:
- PHP >= 7.1.3
  - OpenSSL PHP Extension
  - PDO PHP Extension
  - Mbstring PHP Extension
  - Tokenizer PHP Extension
  - XML PHP Extension
  - Ctype PHP Extension
  - JSON PHP Extension
  - BCMath PHP Extension
- MySQL 5.7
- Nginx or Apache
  - References in setting up:
  - https://laravel.com/docs/5.8/installation#web-server-configuration
- Postman for testing
  - https://www.getpostman.com/downloads

## Installation Instructions:
1. Run composer `composer install`
2. Create Database in MySQL `mysql -u root -e "create database lucky_shine";`
3. Create or copy env file - `php -r "file_exists('.env') || copy('.env.example', '.env');"`
4. Setup ENV file with the correct configuration (Database, etc.)
5. Run `php artisan migrate --pretend` to test the migration
    - If fails with these condition
      - No application key - Run `php artisan key:generate --ansi`
6. Run `php artisan migrate --seed` to run the migration
7. If you dont have any webserver, you can run it using the PHP builtin webserver
   - Run `php artisan serve`
8. If you have a webserver, Apply the configurations above listed
   - Reference: https://laravel.com/docs/5.8/installation#web-server-configuration
9. Restart the webserver
10. Import the Postman collection - (`postman` folder)
    - Download Link: https://www.getpostman.com/downloads
11. Test and enjoy the application
    - Get treasures around a specific point (`GET api/treasures`)
      - Fields:
        - `latitude` - Latitude
        - `longitude` - Longitude
        - `distance` - Distance in kilometers (1 or 10)
    - Get treasures around a specific point with minimum prize_value (`GET api/treasures`)
      - Fields:
        - `latitude` - Latitude
        - `longitude` - Longitude
        - `distance` - Distance in kilometers (1 or 10)
        - `prize_value` - Treasure prize value filter (between 10 to 30) (Optional)
    - Collect treasures using a User `user_id` and the Treasure `treasure_id` (`POST api/treasures/{treasure_id}`)
      - Fields:
        - `user_id` - User to collect to (Mock logged in user)
        - `latitude` - Latitude
        - `longitude` - Longitude

# TODO/Improvements:
1. Create Authentication
2. Integrate user on the `Treasure List` endpoint (Authorization)
3. Create `Treasure List` filtering on specific logged in user (query against the collected treasures and blacklist it for the day)
4. Refactor filters for `Treasure Collect` - Put it as a separate file
5. Separate Geo Helper `isNearby`
6. etc..