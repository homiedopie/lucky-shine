<?php

use Illuminate\Database\Seeder;

class MoneyValuesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $money_values = [
            [ 'treasure_id' => 100, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 101, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 102, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 103, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 104, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 105, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 106, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 107, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 108, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 109, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 110, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 111, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 112, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 113, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 114, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 115, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 116, 'amount' => 10, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 117, 'amount' => 15, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 100, 'amount' => 20, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 101, 'amount' => 25, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 102, 'amount' => 20, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 103, 'amount' => 25, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 107, 'amount' => 30, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 108, 'amount' => 30, 'created_at' => \Carbon\Carbon::now() ],
            [ 'treasure_id' => 109, 'amount' => 30, 'created_at' => \Carbon\Carbon::now() ],
        ];

        \App\Models\MoneyValue::insert($money_values);
    }
}
