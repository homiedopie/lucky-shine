<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'id' => 3000,
                'name' => 'U1',
                'age' => 21,
                'password' => Hash::make('luckyshine001'),
                'email' => 'u1@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3001,
                'name' => 'U2',
                'age' => 51,
                'password' => Hash::make('luckyshine002'),
                'email' => 'u2@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3002,
                'name' => 'U3',
                'age' => 31,
                'password' => Hash::make('luckyshine003'),
                'email' => 'u3@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3003,
                'name' => 'U4',
                'age' => 18,
                'password' => Hash::make('luckyshine004'),
                'email' => 'u4@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3004,
                'name' => 'U5',
                'age' => 21,
                'password' => Hash::make('luckyshine005'),
                'email' => 'u5@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
            [
                'id' => 3005,
                'name' => 'U6',
                'age' => 35,
                'password' => Hash::make('luckyshine006'),
                'email' => 'u6@luckyshine.xyz',
                'created_at' => \Carbon\Carbon::now()
            ],
        ];

        \App\Models\User::insert($users);
    }
}
