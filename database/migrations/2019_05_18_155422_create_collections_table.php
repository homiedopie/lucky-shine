<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('collections', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('amount');
            $table->unsignedBigInteger('treasure_id');
            $table->unsignedBigInteger('money_value_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamp('collected_at');
            $table->timestamps();

            $table->foreign('treasure_id')->references('id')->on('treasures');
            $table->foreign('money_value_id')->references('id')->on('money_values');
            $table->foreign('user_id')->references('id')->on('users');

            $table->index(['collected_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('collections');
    }
}
